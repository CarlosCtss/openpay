import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http'
import {Usuario} from '../Modelo/Usuario';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ServiceService {


  constructor(private http:HttpClient) { 
  }

      
  private headers = new HttpHeaders({
    authorization : 'Basic ' + btoa('admin : admin'),
    'Content-type' : 'application/json'
  });


  Url = 'http://localhost:8081/usuarios';

/*
  getAuth(){

    let a = this.http.get(this.Url+"/auth",{headers:this.headers});
    console.log(a);
    return a;
  }*/


/*
  login(): Observable<any> {
    const urlEndpoint = 'http://localhost:8081/usuarios/auth';

    const credenciales = btoa('admin' + ':' + 'admin');

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Basic ' + credenciales
    });

    let params = new URLSearchParams();
    params.set('grant_type', 'password');
    params.set('username', "admin");
    params.set('password', "admin");
    console.log(params.toString());
    let a = this.http.post<any>(urlEndpoint, params.toString(), { headers: httpHeaders });
    return a;
  }*/


  getUsuarios(){
    let username ='admin';
    let password ='admin';
    return this.http.get<Usuario[]>(this.Url+"/listar");
  } 

  createUsuario(usuario:Usuario){
    return this.http.post<Usuario>(this.Url,usuario);
  }

  getUsuarioById(id:number){
    return this.http.get<Usuario>(this.Url+"/"+id);
  }

  updateUsuario(usuario:Usuario){
    return this.http.put<Usuario>(this.Url+"/"+usuario.id,usuario);
  }

  deleteUsuario(usuario:Usuario){
    return this.http.delete<Usuario>(this.Url+"/"+usuario.id);
  }
}
