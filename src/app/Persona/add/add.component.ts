import { Component, OnInit } from '@angular/core';
import { ServiceService } from 'src/app/Service/service.service';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/Modelo/Usuario';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  usuario : Usuario=new Usuario();
  constructor(private router:Router,private service:ServiceService) { }

  ngOnInit(): void {
  }

  Guardar(){
    this.service.createUsuario(this.usuario)
    .subscribe(data=>{
      alert("Usuario guardado");
      this.router.navigate(['listar']);
    });
  }

  Cancelar(){
    this.router.navigate(['listar']);
  }

}
